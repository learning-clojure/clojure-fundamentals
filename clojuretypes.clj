(ns learnclojure)

(type 1)
(type (int 1))
(type 1.1)
(type true)
(type "Hello")

(type :a)
(type (keyword "a"))

(type (quote a))
(type 'a')

(type (list 1 2 3))

(type (vector 1 2 3))
(type [1 2 3])

(nth (vector 1 2 3) 2)

(first (list 1 2 3))
(last (list 1 2 3))

(type {:a 1 :b 1 :c 1})
(type (hash-map :a 1 :b 2))
(:a {:a 1 :b 2})

(type #{1 2 3 4})
(type (hash-set 1 2 3 4))
