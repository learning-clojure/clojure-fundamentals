(ns learnclojure)

(def x "Hello Clojure")

(let [x "Eduard"]
  (println "Hello " x))

(str x)

(def x "Hello")

(if (empty? x)
  "X is empty"
  "X is not empty")

(if nil "Yes" "No")

(if (empty? x)
  nil
  (do
    (println "Ok")
    :ok))

(if-not (empty? x)
  (do
    (println "Ok")
    :ok))

(when-not (empty? x)
  (println "Ok")
  :ok)

(when (not (empty? x)) :ok)

(case x
  "Goodbye" :goodbye
  "Hello" :hello
  :nothing)

(case x
  "Goodbye" :goodbye
  "Hi" :hello
  :nothing)

(reverse x)
(apply str (reverse x))

(cond
  (= x "Goodbye") :goodbye
  (= (apply str (reverse x)) "olleH") :olleh
  :otherwise :nothing)