(ns learnclojure)

(fn [] "Hello")

(type (fn [] "Hello"))

((fn [] "Hello"))

(def hello (fn [] "Hello"))
(hello)

(#(str "Hello"))

(defn hello [] "Hello")

(defn hello [name] (str "Hello, " name))
(hello "Edu")

(defn hello [name title] (str "Hello, " title " " name))
(hello "Frances" "Admiral")

(defn hello "Greets a person named <name> with their <title>" [name title] (str "Hello, " title " " name))

(require '[clojure.repl :refer [doc]])
(doc hello)
(hello)

(defn hello [& args]
  (str "Hello, " (apply str args)))

(hello "Eduard" "Janna")

(defn hello
  ([] "Hello, World")
  ([name] (str "Hello, " name)))
(hello)
(hello "Arno")

(defn hello
  ([] (hello "World"))
  ([name] (str "Hi, " name)))
(hello)
(hello "Lauri")

(defn hello [config]
  (str "Hello, " (:name config)))
(hello {:name "Kristian"})

(defn hello [{name :name}]
  (str "Hello, " name))
(hello {:name "Karoliina"})

(defn hello [[name title]]
  (str "Hello, " title " " name))
(hello ["Lucy" "Admiral"])
